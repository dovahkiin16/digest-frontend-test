<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * $this->call(DigestSeeder::class);
     * Seed the application's database.
     * @return void
     
     */
    public function run(){
        $this->call(DigestSeeder::class);
    }
    
}
